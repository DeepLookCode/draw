<?

namespace Deeplook\Draw;

use Bitrix\Main\Entity,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Entity\Validator\Length;


Loc::loadMessages(__FILE__);

class DrawTable extends Entity\DataManager
{
    public static function getTableName()
    {
        return 'deeplook_draw';
    }

    public static function getMap()
    {
        return array(
            new Entity\IntegerField("ID", array(
                "primary" => true,
                "autocomplete" => true
            )),
            new Entity\StringField("NAME", [
                'required' => true,
                'validation' => function(){
                    return array(
                        new Length(null, 255)
                    );
                }
            ]),
            new Entity\StringField("FILE_NAME", [
                'required' => true,
                'validation' => function(){
                    return array(
                        new Length(null, 255)
                    );
                }
            ]),
            new Entity\StringField("PASSWORD_HASH", [
                'required' => true,
                'validation' => function(){
                    return array(
                        new Length(null, 512)
                    );
                }
            ])
        );
    }
}