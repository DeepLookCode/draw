<?

namespace Deeplook\Draw;

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Config\Option,
    Bitrix\Main\Web\Json;

class DrawLib
{
    static $availableTypes = ['jpg', 'jpeg', 'gif', 'png']; // available types for image

    static function base64ToImage($data, $output_file)
    {
        $imageFilesDirectory = Option::get("deeplook.draw", "imageFilesPath", "/upload/deeplook.draw/");

        if (preg_match('/^data:image\/(\w+);base64,/', $data, $type)) {
            $data = substr($data, strpos($data, ',') + 1);
            $type = strtolower($type[1]); // jpg, png, gif

            if (!in_array($type, self::$availableTypes)) {
                return Json::encode(['response' => 'error', 'message' => Loc::getMessage('INVALID_IMAGE_TYPE')]);
            }

            $data = base64_decode($data);

            if ($data === false) {
                return Json::encode(['response' => 'error', 'message' => Loc::getMessage('BASE64_DECODE_FAILED')]);
            }
        } else {
            return Json::encode(['response' => 'error', 'message' => Loc::getMessage('DID_NOT_MATCH_DATA_URI')]);
        }

        $fileName = $_SERVER["DOCUMENT_ROOT"].$imageFilesDirectory.$output_file . ".{$type}";
        if (file_put_contents($fileName, $data)) {
            return $output_file.".{$type}";
        } else {
            return false;
        }
    }
}