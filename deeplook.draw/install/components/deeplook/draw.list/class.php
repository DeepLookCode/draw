<?
use Deeplook\Draw\DrawTable,
    Bitrix\Main\Config\Option,
    Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

Loc::loadMessages(__FILE__);

if (!\Bitrix\Main\Loader::includeModule('deeplook.draw'))
{
    ShowError(Loc::getMessage('DEEPLOOK_DRAW_MODULE_NOT_INSTALLED'));
    return;
}

class DrawListComponent extends CBitrixComponent
{
    private $imageFilesDirectory = '/upload/deeplook.draw/';

    public function getElementsList()
    {
        $res = DrawTable::getList();
        $list = $res->fetchAll();

        if(!empty($list))
        {
            $par = $this::getParent()->arParams;

            $path = Option::get("deeplook.draw", "imageFilesPath", "/upload/deeplook.draw/");
            foreach ($list as $key => $value){
                $list[$key]["IMAGE_SRC"] = $path.$value["FILE_NAME"];
                $list[$key]["DETAIL_URL"] = str_replace('#ELEMENT_ID#',$value["ID"], $par["SEF_FOLDER"].$par["SEF_URL_TEMPLATES"]["element"]);
            }

            return $list;
        }else{
            return false;
        }
    }

    public function executeComponent()
    {
        $this->arResult = $this->getElementsList();
        $this->includeComponentTemplate();
    }
}