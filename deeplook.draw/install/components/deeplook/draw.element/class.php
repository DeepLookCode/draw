<?
use Deeplook\Draw\DrawTable,
    Deeplook\Draw\DrawLib,
    Bitrix\Main\Context,
    Bitrix\Main\Web\Json,
    Bitrix\Main\IO\File,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Config\Option;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

Loc::loadMessages(__FILE__);

if (!\Bitrix\Main\Loader::includeModule('deeplook.draw'))
{
    ShowError(Loc::getMessage('DEEPLOOK_DRAW_MODULE_NOT_INSTALLED'));
    return;
}

class DrawElementComponent extends CBitrixComponent
{
    private $imageFilesDirectory = '';
    private $availableTypes = ['jpg', 'jpeg', 'gif', 'png']; // available types for image

    private $imgId = '';
    private $imgbase64 = '';
    private $imageName = '';
    private $fileName = '';
    private $oldFileName = '';
    private $imagePassword = '';

    private $error = [];

    function __construct($component = null)
    {
        // get image directory
        $this->imageFilesDirectory = Option::get("deeplook.draw", "imageFilesPath", "/upload/deeplook.draw/");
        parent::__construct($component);
    }

    public function onPrepareComponentParams($params)
    {
        if (!empty($params['RESULT']))
        {
            $this->arResult = $params['RESULT'];
            unset($params['RESULT']);
        }

        if (!empty($params['PARAMS']))
        {
            $params += $params['PARAMS'];
            unset($params['PARAMS']);
        }

        return $params;
    }

    public function getElementById($id)
    {
        $res = DrawTable::getById($id);
        if($ar_res = $res->fetch())
        {
            return $ar_res;
        }else{
            return false;
        }
    }

    public function updateImage()
    {
        // get request from bitrix
        $request = Context::getCurrent()->getRequest();
        if(!$request->isPost())
            return Json::encode(['response' => 'error', 'message' => Loc::getMessage('ID_IS_NOT_POST')]);

        // get image id
        $this->imgId = $request->getPost('imgId');
        if(empty($this->imgId))
            $this->error[] = Loc::getMessage('ID_IS_EMPTY');

        // get image in base64
        $this->imgbase64 = $request->getPost('imgBase64');
        if(empty($this->imgbase64))
            $this->error[] = Loc::getMessage('BASE64_IS_EMPTY');

        // get image password
        $this->imagePassword = $request->getPost('password');
        if(empty($this->imagePassword))
            $this->error[] = Loc::getMessage('PASS_IS_EMPTY');

        // get image name
        $this->imageName = $request->getPost('imageName');
        if(empty($this->imageName))
            $this->error[] = Loc::getMessage('IMAGE_NAME_IS_EMPTY');

        // return response
        if(empty($this->error)){
            $this->fileName = md5(time() . serialize($_SERVER['POST']));

            $res = DrawTable::getById($this->imgId);
            if(!$oldElem = $res->fetch())
                return Json::encode(['response' => 'error', 'message' => Loc::getMessage('ELEMENT_NOT_FOUND')]);

            $hash = $oldElem['PASSWORD_HASH'];

            $verify = password_verify($this->imagePassword, $hash);
            if(!$verify)
                return Json::encode(['response' => 'error', 'message' => Loc::getMessage('PASSWORD_IS_WRONG')]);

            // add file
            $this->oldFileName = $oldElem['FILE_NAME'];
            if($imageAddFilename = DrawLib::base64ToImage($this->imgbase64,$this->fileName)) {
                $del = File::deleteFile($_SERVER["DOCUMENT_ROOT"].$this->imageFilesDirectory.$this->oldFileName);

                // if delete success - update element, return json
                if($del) {
                    $result = DrawTable::update(
                        $this->imgId,
                        [
                            "NAME" => $this->imageName,
                            "FILE_NAME" => $imageAddFilename,
                        ]
                    );
                    return Json::encode([
                        'response' => 'success',
                        'message' => Loc::getMessage('IMAGE_WAS_SAVE'),
                        'data' => [
                            'id' => $result->getId()
                        ]
                    ]);
                }else{
                    return Json::encode(['response' => 'error', 'message' => Loc::getMessage('ERORROR_IMAGE_UPDATE')]);
                }
            }else{
                return Json::encode(['response' => 'error', 'message' => Loc::getMessage('ERORROR_IMAGE_SAVE')]);
            }
        }else{
            return Json::encode(['response' => 'error', 'message' => $this->error]);
        }
    }

    public function executeComponent()
    {
        $elemId = $this->arParams["ELEMENT_ID"];

        if($elemId > 0){
            $res = $this->getElementById($elemId);

            if($res){
                $path = Option::get("deeplook.draw", "imageFilesPath", "/upload/deeplook.draw/");
                $res['FILE_PATH'] = $path.$res['FILE_NAME'];
                $this->arResult = $res;
            }
        }

        $this->includeComponentTemplate();
    }
}