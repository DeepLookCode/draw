<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);

$this->addExternalJS($templateFolder."/js/jquery-3.3.1.min.js");

$this->addExternalCss($templateFolder."/js/drawingboard/drawingboard.css");
$this->addExternalJS($templateFolder."/js/drawingboard/drawingboard.js");

$this->addExternalCss($templateFolder."/css/template.css");
?>

<div id="picture"></div>
<form id="pictureForm" action="" >
    <label for="formName">Наименование</label>
    <input id="formName" type="text" name="name" value="<?=$arResult['NAME']?>">

    <label for="formPass">Пароль файла</label>
    <input id="formPass" type="password" name="password"/>

    <input type="submit" value="Отправить" />
</form>

<script>
    // JS Draw lib init
    var myBoard = new DrawingBoard.Board('picture',{
        controls: [
            { Size: { type: 'dropdown' } },
            { DrawingMode: { filler: false } }
        ],
        webStorage: false
    });

    // Set image for canvas
    myBoard.setImg('<?=$arResult["FILE_PATH"];?>');

    // Send data to ajax.php
    $('#pictureForm').submit(function(){
        var dataURL = myBoard.canvas.toDataURL();
        var imageName = $('#formName').val();
        var imagePass = $('#formPass').val();
        var imgId = <?=$arResult['ID'];?>;

        $.ajax({
            type: "POST",
            url: "<?=$component->getPath();?>/ajax.php",
            data: {
                imgId: imgId,
                imgBase64: dataURL,
                imageName: imageName,
                password: imagePass
            }
        }).done(function(response) {
            if(response.message){
                alert(response.message);
            }
        });

        return false;
    });
</script>