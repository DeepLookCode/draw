<?
$arComponentParameters = array(
    "PARAMETERS" => array(
        "ELEMENT_ID" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("IBLOCK_ELEMENT_ID"),
            "TYPE" => "STRING",
            "DEFAULT" => '={$_REQUEST["ELEMENT_ID"]}',
        ),
    )
);