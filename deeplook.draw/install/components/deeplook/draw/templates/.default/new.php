<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\ModuleManager;

/**
 * @global CMain $APPLICATION
 * @var CBitrixComponent $component
 * @var array $arParams
 * @var array $arResult
 */

$this->setFrameMode(true);

// if modele is install
if (ModuleManager::isModuleInstalled("deeplook.draw"))
{
    $APPLICATION->IncludeComponent(
        "deeplook:draw.new",
        "",
        Array(),
        $component
    );?>

    <a class="button-list" href="<?=$arParams["SEF_FOLDER"].$arParams["SEF_URL_TEMPLATES"]["list"]?>">К списку изображений</a>
<?}else{
    echo Loc::getMessage('MODULE_NOT_INSTALL');
}