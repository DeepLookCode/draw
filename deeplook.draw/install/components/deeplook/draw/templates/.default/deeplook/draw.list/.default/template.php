<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);
$this->addExternalCss($templateFolder."/css/template.css");

if(!$arResult){
    echo "<div>".Loc::getMessage('LIST_IS_EMPTY')."</div>";
}else{?>
    <div class="wrapper">
        <?
        foreach ($arResult as $item){?>
            <div class="item-wrapper">
                <a class="item" href="<?=$item['DETAIL_URL'];?>">
                    <div class="image" style="background-image: url(<?=$item['IMAGE_SRC']?>);"></div>
                    <div class="description"><?=$item['NAME']?></div>
                </a>
            </div>
        <?}?>
        <div style="clear: both"></div>
    </div>
<?}?>

