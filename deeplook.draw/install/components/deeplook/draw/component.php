<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

// set default url templates
$arDefaultUrlTemplates404 = array(
    "list" => "index.php",
    "element" => "#ELEMENT_ID#.php",
    "new" => "new.php"
);

$arDefaultVariableAliases404 = array();

$arDefaultVariableAliases = array();

$arComponentVariables = array("ELEMENT_ID");


$SEF_FOLDER = "";
$arUrlTemplates = array();

$arVariables = array();

$arUrlTemplates =
    CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplates404,
        $arParams["SEF_URL_TEMPLATES"]);
$arVariableAliases =
    CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases404,
        $arParams["VARIABLE_ALIASES"]);

$componentPage = CComponentEngine::ParseComponentPath(
    $arParams["SEF_FOLDER"],
    $arUrlTemplates,
    $arVariables
);

if (StrLen($componentPage) <= 0)
    $componentPage = "list";

CComponentEngine::InitComponentVariables($componentPage,
    $arComponentVariables,
    $arVariableAliases,
    $arVariables);

$SEF_FOLDER = $arParams["SEF_FOLDER"];

$arResult = array(
    "FOLDER" => $SEF_FOLDER,
    "URL_TEMPLATES" => $arUrlTemplates,
    "VARIABLES" => $arVariables,
    "ALIASES" => $arVariableAliases
);

$this->IncludeComponentTemplate($componentPage);
?>