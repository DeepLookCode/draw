<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("DRAW_COMPONENT_NAME"),
    "DESCRIPTION" => GetMessage("DRAW_COMPONENT_DESCRIPTION"),
    "ICON" => "/images/catalog.gif",
    "COMPLEX" => "Y",
    "SORT" => 10,
    "PATH" => array(
        "ID" => "content",
        "CHILD" => array(
            "ID" => "deeplook",
            "NAME" => GetMessage("DRAW_COMPONENT_DESC"),
            "SORT" => 10,
        )
    )
);
?>