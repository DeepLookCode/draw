<?
$arComponentParameters = array(
    "PARAMETERS" => array(
        "SEF_MODE" => array(
            "list" => array(
                "NAME" => GetMessage("SECTION_PAGE"),
                "DEFAULT" => "",
                "VARIABLES" => array(),
            ),
            "element" => array(
                "NAME" => GetMessage("DETAIL_PAGE"),
                "DEFAULT" => "#ELEMENT_ID#/",
                "VARIABLES" => array(
                    "ELEMENT_ID"
                ),
            ),
            "new" => array(
                "NAME" => GetMessage("NEW_PAGE"),
                "DEFAULT" => "new/",
                "VARIABLES" => array(),
            ),
        )
    )
);

$arComponentParameters["PARAMETERS"]["VARIABLE_ALIASES"] = array();
$arComponentParameters["PARAMETERS"]["VARIABLE_ALIASES"]["ELEMENT_ID"] = array(
    "NAME" => GetMessage("CP_BC_VARIABLE_ALIASES_ELEMENT_ID"),
    "TEMPLATE" => "#ELEMENT_ID#",
);
?>