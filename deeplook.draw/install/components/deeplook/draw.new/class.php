<?
use Deeplook\Draw\DrawTable,
    Deeplook\Draw\DrawLib,
    Bitrix\Main\Context,
    Bitrix\Main\IO\Directory,
    Bitrix\Main\IO\File,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Web\Json,
    Bitrix\Main\Config\Option;


if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

Loc::loadMessages(__FILE__);

if (!\Bitrix\Main\Loader::includeModule('deeplook.draw')) {
    ShowError(Loc::getMessage('DEEPLOOK_DRAW_MODULE_NOT_INSTALLED'));
    return;
}

class DrawNewComponent extends CBitrixComponent
{
    private $availableTypes = ['jpg', 'jpeg', 'gif', 'png'];
    private $error = [];

    private $imageFilesDirectory = '';

    private $imgbase64 = '';
    private $imageName = '';
    private $imagePassword = '';
    private $fileName = '';

    function __construct($component = null)
    {
        $this->imageFilesDirectory = Option::get("deeplook.draw", "imageFilesPath", "/upload/deeplook.draw/");
        parent::__construct($component);
    }

    public function addNewElement()
    {
        // get request from bitrix
        $request = Context::getCurrent()->getRequest();
        if(!$request->isPost())
            return Json::encode(['response' => 'error', 'message' => Loc::getMessage('ID_IS_NOT_POST')]);

        // if directory does not exist - create
        if(!Directory::isDirectoryExists($_SERVER["DOCUMENT_ROOT"].$this->imageFilesDirectory)){
            Directory::createDirectory($_SERVER["DOCUMENT_ROOT"].$this->imageFilesDirectory);
        }

        // get image in base64
        $this->imgbase64 = $request->getPost('imgBase64');
        if(empty($this->imgbase64))
            $this->error[] = Loc::getMessage('BASE64_IS_EMPTY');

        // get image password
        $this->imagePassword = $request->getPost('password');
        if(empty($this->imagePassword))
            $this->error[] = Loc::getMessage('PASS_IS_EMPTY');

        // get image name
        $this->imageName = $request->getPost('imageName');
        if(empty($this->imageName))
            $this->error[] = Loc::getMessage('IMAGE_NAME_IS_EMPTY');

        // return response
        if(empty($this->error)){
            $this->fileName = md5(time() . serialize($_SERVER['POST']));

            // add file
            if($imageAddFilename = DrawLib::base64ToImage($this->imgbase64,$this->fileName)){
                // add entity to DB
                $result = DrawTable::add([
                    "NAME" => $this->imageName,
                    "FILE_NAME" => $imageAddFilename,
                    // use password_hash for password storage
                    "PASSWORD_HASH" => password_hash($this->imagePassword,PASSWORD_DEFAULT)
                ]);

                if($result->isSuccess()){
                    return Json::encode([
                        'response' => 'success',
                        'message' => Loc::getMessage('IMAGE_WAS_SAVE'),
                        'data' => [
                            'id' => $result->getId()
                        ]
                    ]);
                }else{
                    File::deleteFile($_SERVER["DOCUMENT_ROOT"].$this->imageFilesDirectory.$this->fileName);

                    return Json::encode([
                        'response' => 'error',
                        'message' => Loc::getMessage('CANT_SAVE')
                    ]);
                }
            }else{
                return Json::encode(['response' => 'error', 'message' => Loc::getMessage('ERORROR_IMAGE_SAVE')]);
            }
        }else{
            return Json::encode(['response' => 'error', 'message' => $this->error]);
        }
    }

    public function executeComponent()
    {
        $this->includeComponentTemplate();
    }
}