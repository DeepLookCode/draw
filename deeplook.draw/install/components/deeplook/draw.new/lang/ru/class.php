<?
$MESS["DEEPLOOK_DRAW_MODULE_NOT_INSTALLED"] = "Модуль \"Рисование\" не установлен";

$MESS["ID_IS_NOT_POST"] = "Запрос должен быть типа POST";
$MESS["ID_IS_EMPTY"] = "ID не отправлен";
$MESS["BASE64_IS_EMPTY"] = "Base64 не отправлен";
$MESS["PASS_IS_EMPTY"] = "Base64 не отправлен";
$MESS["IMAGE_NAME_IS_EMPTY"] = "Base64 не отправлен";

$MESS["ELEMENT_NOT_FOUND"] = "Элемент не найден";
$MESS["PASSWORD_IS_WRONG"] = "Неверный пароль";

$MESS["IMAGE_WAS_SAVE"] = "Изображение сохранено";
$MESS["CANT_SAVE"] = "Изображение сохранено";

$MESS["ERORROR_IMAGE_UPDATE"] = "Ошибка обновления ихображения";
$MESS["ERORROR_IMAGE_SAVE"] = "Ошибка сохранения ихображения";

$MESS["INVALID_IMAGE_TYPE"] = "Неверный тип изображения";
$MESS["BASE64_DECODE_FAILED"] = "Невозможно декодировать строку";
$MESS["DID_NOT_MATCH_DATA_URI"] = "Не соответствует изображению";
?>