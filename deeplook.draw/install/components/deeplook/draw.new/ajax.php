<?
/** @global CMain $APPLICATION */
define('STOP_STATISTICS', true);

require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

require_once('class.php');

$drawNewComponent = new DrawNewComponent();
$resp = $drawNewComponent->addNewElement();

header('Content-Type: application/json');
echo $resp;