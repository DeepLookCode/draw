CREATE TABLE if not exists deeplook_draw (
  ID int(11) NOT NULL AUTO_INCREMENT,
  NAME varchar(255) NOT NULL,
  FILE_NAME varchar(255) NOT NULL,
  PASSWORD_HASH varchar(512) NOT NULL,
  PRIMARY KEY (ID)
);