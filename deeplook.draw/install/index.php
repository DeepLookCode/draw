<?php

use Bitrix\Main\Config\Option;

IncludeModuleLangFile(__FILE__);

if (class_exists("deeplook_draw"))
    return;

class deeplook_draw extends CModule
{
    var $MODULE_ID = "deeplook.draw";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;

    function __construct()
    {
        $arModuleVersion = array();

        include(dirname(__FILE__)."/version.php");

        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];

        $this->MODULE_NAME = GetMessage("DEEPLOOK_APPLICATION_INSTALL_NAME");
        $this->MODULE_DESCRIPTION = GetMessage("DEEPLOOK_APPLICATION_INSTALL_DESCRIPTION");

        $this->PARTNER_NAME = GetMessage("DEEPLOOK_APPLICATION_PARTNER");
        $this->PARTNER_URI = "http://deeplook.xyz/";
    }

    function setOptions()
    {
        Option::set("deeplook.draw", "imageFilesPath", "/upload/deeplook.draw/");
    }

    function unsetOptions()
    {
        Option::delete("deeplook.draw", "imageFilesPath");
    }

    function InstallDB()
    {
        global $DB, $DBType;

        $DB->RunSQLBatch(dirname(__FILE__) . "/sql/" . $DBType . "/install.sql");

        \Bitrix\Main\ModuleManager::registerModule($this->MODULE_ID);

        //* USER FIELDS (end) *//
        return true;
    }

    function UnInstallDB()
    {
        global $DB,$DBType;

        $DB->RunSQLBatch(dirname(__FILE__) . "/sql/" . $DBType . "/uninstall.sql");

        \Bitrix\Main\ModuleManager::unRegisterModule($this->MODULE_ID);

        return true;
    }

    function InstallFiles()
    {
        CopyDirFiles(dirname(__FILE__)."/components", $_SERVER["DOCUMENT_ROOT"]."/local/components", true, true);

        return true;
    }

    function UnInstallFiles()
    {
        DeleteDirFiles(dirname(__FILE__)."/components", $_SERVER["DOCUMENT_ROOT"]."/local/components");

        return true;
    }

    function DoInstall()
    {
        $this->InstallFiles();
        $this->InstallDB();
        $this->setOptions();

        return true;
    }

    function DoUninstall()
    {
        $this->UnInstallDB();
        $this->UnInstallFiles();
        $this->unsetOptions();

        return true;
    }
}